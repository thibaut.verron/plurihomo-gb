"""
Output of selection function:
list of tuples (c, mons, sigs) where:
- c is a description of the step (not used by the algo)
- mons is the support of the polynomials appearing at the step
- sigs is the set of signatures processed at the step
"""

def algosteps_whomo(F, dmax, W, dmin=None):
    R = F[0].parent() # not 100% robust if F[0] is 0
    n = R.ngens()
    dd = [R(f).weighted_degree(W) for f in F]
    res = []
    if dmin is None:
        dmin = min(dd)
    for d in range(dmin, dmax+1):
        resd = []
        mons = [R.monomial(*e)
                for e in tuples_of_weighted_degree(W,d,dmax=dmax)]
        mons.sort()
        mons.reverse()
        for j in range(len(F)):
            if F[j] == 0:
                pass
            else:
                mm = [R.monomial(*e)
                      for e in tuples_of_weighted_degree(W,d-dd[j],dmax=dmax)]
                mm.sort()
                for m in mm:
                    resd.append((j,m))
        #print("{} -> {}".format(d,resd))
        res.append((d,mons,resd))
    return res

# Tests
#########
# If one of the F is 0, it shouldn't appear in the signatures
if not notest:
    R.<x,y> = QQ[]
    W = [1,2]
    # Bug if one function is 0
    # It shouldn't appear in the signatures
    F = [x^4+x^2*y+y^2, 0]
    algosteps = algosteps_whomo(F, 6, W)
    assert all(all(s[0] != 1 for s in step[2]) for step in algosteps)
    # Bug with duplicate entries
    R.<x,y> = QQ[]
    W = [1,1]
    F = [x*y^2, x*y^2, x*y^2, x*y^2]
    algosteps = algosteps_whomo(F, 6, W)
    descs = [step[0] for step in algosteps]
    assert len(descs) == len(set(descs))


def algosteps_homo(F, dmax):
    W = [1]*len(F)
    return algosteps_whomo(F, dmax, W)


def algosteps_bihomo(F, dmax, blocks):
    R = F[0].parent()
    n = R.ngens()
    W = [1]*n
    W1 = [1]*blocks[0] + [0]*blocks[1]
    W2 = [0]*blocks[0] + [1]*blocks[1]
    dd = [f.degree() for f in F]
    dd1 = [R(f).weighted_degree(W1) for f in F]
    dd2 = [R(f).weighted_degree(W2) for f in F]
    res = []
    for d in range(min(dd), dmax+1):
        for d1 in range(0,d+1):
            resd1 = []
            d2 = d - d1
            mons = [R.monomial(*e)
                    for e in tuples_of_matrix_weighted_degree([W,W1,W2],[d,d1,d2],dmax=d)]
            mons.sort()
            mons.reverse()

            for j in range(len(F)):
                mm = [R.monomial(*e)
                      for e in tuples_of_matrix_weighted_degree([W1, W2],
                                                               [d1-dd1[j],d2-dd2[j]],
                                                               dmax=d-dd[j])]
                for m in mm:
                    resd1.append((j,m))
            #print("{} -> {}".format((d,(d1,d2)),resd1))
            res.append(((d,(d1,d2)),mons,resd1))
    return res

def split_list(L, key):
    """
    Split L according to the value of key
    """
    buckets = {}
    for x in L:
        k = key(x)
        if k in buckets:
            buckets[k].append(x)
        else:
            buckets[k] = [x]
    return buckets
    
def algosteps_mwhomo(F, dmax, WW, last_order="grevlex"):
    # Requires that for the first system of weights, the weights are all
    # positive or integers.
    R = F[0].parent()
    n = R.ngens()

    # If the polynomials are not homogeneous, fail
    if any(not is_weighted_homogeneous(f,w) for f in F for w in WW):
        raise NotImplementedError("the system must be matrix-weighted homogeneous")

    # Unused for now, the construction should ensure that the ordering is correct
    sort_matrix = weights_to_ordering_matrix(WW, last_order=last_order)
    def sortkey_mons(mon):
        return sort_matrix*(mons.exponents()[0])
    def sortkey_sigs(s):
        i,m = s
        return (i,sortkey_mons(m))

    dd = [[R(f).weighted_degree(W) for f in F] for W in WW]
    L0 = algosteps_whomo(F,dmax,WW[0])
    # Reformat a bit to make the index a tuple
    L0 = [((d,),mons,sigs) for d,mons,sigs in L0]
    for i in range(1,len(WW)):
        #print(L0)
        L1 = []
        W = WW[i]
        def key_mon(m):
            return m.weighted_degree(W)
        def key_sig(s):
            si, sm = s
            return sm.weighted_degree(W) + dd[i][si]
        
        for idx,mons,sigs in L0:
            split_mons = split_list(mons, key_mon)
            split_sigs = split_list(sigs, key_sig)
            # Not sure if all monomials are reachable, so we use the keys from
            # the sigs
            degs = list(split_sigs.keys())
            degs.sort()
            for d in degs:
                if d in split_mons:
                    L1.append((idx+(d,), split_mons[d], split_sigs[d]))
                # doing nothing otherwise should be safe
                # actually why is it happening?
            L0 = L1
    return L0

if not notest:
    R.<x,y,z> = QQ[]
    F = [random_mwhomo(R,[[1,1,0],[0,0,1]],[1,1], dmax=3) for _ in range(3)]
    print([is_weighted_homogeneous(f,[1,1,0]) for f in F])
    print([is_weighted_homogeneous(f,[0,0,1]) for f in F])
    print([is_weighted_homogeneous(f,[1,1,1]) for f in F])
    print([f.weighted_degree([1,1,0]) for f in F])
    print([f.weighted_degree([0,0,1]) for f in F])
    print([f.weighted_degree([1,1,1]) for f in F])
    print("---")
    for step in algosteps_homo(F,3): print(step)
    print("---")
    for step in algosteps_bihomo(F,3,[2,1]): print(step)
    print("---")
    for step in algosteps_whomo(F,5,[1,2,2]): print(step)
    print("---")
    for step in algosteps_mwhomo(F,3,[[1,1,1],[1,1,0],[0,0,1]]): print(step)
    print("---")
    for step in algosteps_mwhomo(F,3,[[1,1,1],[2,2,1],[1,1,2]]): print(step)

def filter_stepslist(L, filter_gcd=True):
    to_keep = [True for _ in L]
    for i in range(len(L)):
        idx,mons,sigs = L[i]
        if len(mons) == 0 or len(sigs) == 0:
            # Remove trivial steps
            to_keep[i] = False
        elif filter_gcd and gcd(s[1] for s in sigs) != 1:
            # Remove steps which are shifted copies of a previous step
            to_keep[i] = False
    LL = [L[i] for i in range(len(L)) if to_keep[i]]
    return LL
