load("helper.sage")
load_notest("algosteps.sage")

def F5_elimination(M, pivots=None):
    """
    Compute the F5 elimination (Gaussian elimination without swapping rows
    and columns) of the matrix M.

    pivots is a dict such that pivots[i] is the row having column i as pivot

    The output is the list of all rows which were modified at least once.
    """
    nrows, ncols = M.dimensions()
    if pivots is None:
        pivots = {}
    npivots = len(pivots)
    reduced = set({})
    #print(pivots)
    for row in range(len(pivots),nrows):
        # If we already have more pivots than columns, the remaining rows will
        # be semi-trivial divisors of 0'
        # NOTE: They will still be counted as divisors of 0 in the verbose output in
        # case they have signature (1,i). (FIXME)
        if len(pivots) >= ncols: 
            for r in range(row,nrows):
                M[r] = 0
            break
        #print(row,pivots)
        head = True # did we reach the pivot of this row?
        for col in range(ncols):
            if M[row][col] != 0:
                if col in pivots:
                    M.add_multiple_of_row(row,pivots[col],-M[row][col], start_col=col)
                    if head:
                        reduced.add(row)
                elif head:
                    pivots[col] = row
                    M.set_row_to_multiple_of_row(row,row,1/M[row][col])
                    head = False
    # print(M)
    return list(reduced)

def rowpol(row,mons):
    res = 0
    lm = 0
    for i in range(len(row)):
        if row[i] != 0:
            if lm == 0:
                lm = mons[i]
            res += row[i] * mons[i]
    return res, lm


def polrow(pol,idx):
    row = [0 for i in range(len(idx))]
    for c,m in pol:
        row[idx[m]] = c
    return row

"""
Ordering:

Multi-weights + revlex (or grevlex) cannot be described by a square matrix,
which sage/singular requires.  Also sage doesn't have revlex as a tie
breaker. As a workaround, we make our own matrices and pass it to all the
sorting functions.  """

def weights_to_ordering_matrix(Wtup, last_order="grevlex"):
    WW = [list(W) for W in Wtup]
    n = len(WW[0])
    if last_order == "grevlex":
        lastM = [[1 if i < n-j else 0 for i in range(n)] for j in range(n)]
    elif last_order == "revlex":
        lastM = [[1 if i < n-j else 0 for i in range(n)] for j in range(1,n)]
    else:
        raise NotImplementedError
    return Matrix(WW + lastM)

def cmp_mtx(a,b,M):
    ea = vector(a.exponents()[0])
    eb = vector(b.exponents()[0])
    mea = M*ea
    meb = M*eb
    if mea < meb:
        return -1
    elif mea > meb:
        return 1
    else:
        return 0

def lt_mtx(p,M):
    if p == 0:
        return 0
    else:
        L = list(p)
        tm = L[0]
        for i in range(1,len(L)):
            if cmp_mtx(L[i][1], tm[1], M) == 1:
                tm = L[i]
    return tm[0]*tm[1]

def lm_mtx(p,M):
    t = lt_mtx(p,M)
    if t == 0:
        return 0
    else:
        return t.lm()

def lc_mtx(p,M):
    t = lt_mtx(p,M)
    if t == 0:
        return 0
    else:
        return t.lc()

@parallel(ncpus=ncpus)
def reduce(f,G,M):
    ltG = [lt_mtx(g,M) for g in G]
    h = 0
    while f != 0:
        done = False
        while not done and f != 0:
            t = lt_mtx(f,M)
            cands = [i for i in range(len(G)) if ltG[i].divides(t)]
            if not cands:
                done = True
            else:
                i = min(cands)
                f -= t//ltG[i] * G[i]
        t = lt_mtx(f,M)
        h += t
        f -= t
    return h

def spoly(f,g,M):
    ltf = lt_mtx(f,M)
    ltg = lt_mtx(g,M)
    tt = lcm(ltf,ltg)
    if tt.degree() == ltf.degree() + ltg.degree():
        # Buchberger's criterion, skipping the reductions
        return 0 
    else:
        return (tt//ltf)*f - (tt//ltg)*g

def reduce_GB(G,M):
    for i in range(len(G)):
        Gred = G[:i] + G[i+1:]
        G[i] = reduce(G[i],Gred,M)
    return [g for g in G if g != 0]

def is_GB(G,M):
    for i in range(len(G)):
        print("Checking... {}/{}".format(i+1,len(G)))
        for j in range(i+1,len(G)):
            h = spoly(G[i],G[j],M)
            if reduce(h,G,M) != 0:
                return False
    return True

def is_GB_parallel(G,M,WW):
    # We group the checks with the same i so that we get an indication of progress
    for i in range(len(G)):
        print("Checking... {}/{}".format(i+1,len(G)))
        argslist = []
        for j in range(i+1,len(G)):
            h = spoly(G[i],G[j],M)
            argslist.append((h,G,M))
        res = reduce(argslist)
        if any(s[1] != 0 for s in res):
            return False
    return True

# Homogenization

def add_variables(R,n,names):
    Rh = PolynomialRing(R,n,names)
    new = Rh.gens()
    phi = Rh.flattening_morphism()
    return phi.codomain(), [phi(v) for v in new]

def whomogenize(f,W,h):
    # Aux function, does not change the weights
    d = f.weighted_degree(W)
    R = f.parent()
    return ((h^d)*f([R.gen(i)/h^W[i] for i in range(R.ngens())])).numerator()

if not notest:
    R.<x,y,z> = QQ[]
    f = x^2 + y^2 + x
    Rh,hom = add_variables(R,1,"h")
    h = hom[0]
    whomogenize(f,[1,1,1,1],h)
            
def multi_whomogenize(f,WW,hh):
    f = hh[0].parent()(f)
    # Deep copy first
    WW = [W[:] for W in WW]
    for i in range(len(WW)):
        for j in range(len(WW)):
            WW[j].append(1 if i==j else 0)
    for i in range(len(WW)):
        f = whomogenize(f,WW[i],hh[i])
    return f,WW

if not notest:
    R.<x,y> = QQ[]
    f = x^2 + y^2 + x
    Rh,hom = add_variables(R,2,"h")
    multi_whomogenize(f,[[1,1],[1,2]],hom)


@parallel(ncpus=ncpus)
def process_step(desc, mons, sigs, K, F, gblms, f5lms, syzlms, use_F5=False):
    t0 = time.perf_counter()
    G_new = []
    gblms_new = []
    actual_sigs = []
    idx = {}
    for col in range(len(mons)):
        idx[mons[col]] = col
    M = matrix(K, nrows=0, ncols=len(mons))
    pivots = {}
    nred0 = 0
    red0 = []
    # rowsig = []
    # sigrow = {}
    for i in range(len(F)):
        MM = M.rows()
        new = []
        reds = []
        sigsi = [s[1] for s in sigs if s[0] == i]
        for ms in sigsi:
            if any([t.divides(ms) for t in f5lms[i]]):
                pass
            elif any([t.divides(ms) for t in syzlms[i]]):
                pass
            else:
                g = F[i]*ms
                MM.append(polrow(g,idx))
                actual_sigs.append((i,ms))
        M = Matrix(K,MM)
        if use_F5:
            F5_elimination(M,pivots=pivots)
        else:
            M.echelonize()
        pivot_columns = []
        for row in M:
            if not row.is_zero():
                col = min(row.nonzero_positions())
            else:
                col = -1
            pivot_columns.append(col)
        for row in range(len(pivot_columns)):
            if pivot_columns[row] == -1:
                nred0 += 1
                if use_F5:
                    red0.append(actual_sigs[row])
                continue
            pivot_monomial = mons[pivot_columns[row]]
            if (not any([lm.divides(pivot_monomial) for lm in gblms])
                and not any([lm.divides(pivot_monomial) for lm in gblms_new])):
                gg, lm = rowpol(M[row],mons)
                G_new.append((gg,lm,i))
                gblms_new.append(lm)
    t = time.perf_counter() - t0
    nfullrank = min(M.nrows(),M.ncols())-M.rank()
    # nred0 = M.nrows()-len(pivot_columns)
    # if use_F5:
    #     red0 = [sigs[i] for i in range(M.nrows()) if ]
    # else:
    #     red0 = None
    return G_new, t, nred0, nfullrank, red0

def matrixF5_parallel(F, stepslist, filter_gcd=True, algo='default'):
    # algo: 'default' or 'F5'
    if algo == 'default':
        use_F5 = False
    elif algo == 'F5':
        use_F5 = True
    else:
        raise ValueError("Invalid value of 'algo'")
    stepslist = filter_stepslist(stepslist, filter_gcd=filter_gcd) # remove trivial steps
    R = F[0].parent()
    K = R.base_ring()
    f5lms = [list() for i in range(len(F))]
    syzlms = [list() for i in range(len(F))]
    G = []
    G_lms = []
    degs = list({step[0][0] for step in stepslist})
    degs.sort()
    total_red0 = 0
    total_rkfall = 0
    cnt_steps = 0
    max_cnt_mtx = 0
    max_mtx_size = 0
    grouped_steps = [[step for step in stepslist if step[0][0]==d] for d in degs]
    for group in grouped_steps:
        t0 = time.perf_counter()
        print("Processing degree {}".format(group[0][0][0]))
        print("Number of submatrices: {}".format(len(group)))
        nrows = [len(s[2]) for s in group]
        ncols = [len(s[1]) for s in group]
        print("Number of columns: {}~{} (avg. {:.2f})".format(min(ncols), max(ncols), RR(mean(ncols))))
        print("Number of rows:    {}~{} (avg. {:.2f})".format(min(nrows), max(nrows), RR(mean(nrows))))
        max_mtx_size = max(max_mtx_size, max([len(s[2])*len(s[1]) for s in group]))
        max_cnt_mtx = max(max_cnt_mtx, len(group))
        cnt_steps += 1
        parallel_args = [(desc, mons, sigs, K, F, G_lms, f5lms, syzlms, {'use_F5':use_F5})
                         for desc,mons,sigs in group]
        res = list(process_step(parallel_args))
        tlist = []
        cnt = 0
        nred0 = 0
        red0 = []
        nfullrank = 0
        for step_res in res:
            G_new,t,nred0_new, nfullrank_new, red0_new = step_res[1]
            nred0 += nred0_new
            nfullrank += nfullrank_new
            if use_F5:
                red0 += red0_new
                for (j,m) in red0_new:
                    syzlms[j].append(m) # syz criterion
            tlist.append(t)
            for (g,lm,i) in G_new:
                G.append(g)
                G_lms.append(lm)
                cnt += 1
                for j in range(i+1,len(F)):
                    f5lms[j].append(lm) # f5 criterion
        print("Step time: total {:.2f} indiv {:.2f}~{:.2f} (avg. {:.2f})"
              .format(time.perf_counter()-t0, min(tlist), max(tlist), mean(tlist)))
        print("New polynomials:    {}".format(cnt))
        if nred0:
            total_red0 += nred0
            total_rkfall += nfullrank
            print("Reductions to zero: {} // Rank defects: {}".format(nred0, nfullrank))
            if use_F5:
                print("... with signatures: {}".format(red0))
    print("Total number of steps: {}".format(cnt_steps))
    print("Max number of matrices: {}".format(max_cnt_mtx))
    print("Max matrix size: {}".format(max_mtx_size))
    print("Total reductions to zero: {}".format(total_red0))
    print("Total rank defects: {}".format(total_rkfall))
    return G
        
if not notest:
    WW = [[1,1,1,1],[1,1,2,3]]
    K = GF(127)
    n = 4
    nw = len(WW)
    WMatrix = Matrix(WW + [[1 if i <= n-j else 0 for i in range(n)] for j in range(nw,n)])
    R.<x,y,z,t> = PolynomialRing(K,4,order=TermOrder(WMatrix))

    dd = [3,5]
    F = [random_multi_whomo(R,WW,dd,dmax=3) for _ in range(4)]
    print(F)

    try:
        dmax = dmax
    except Exception:
        dmax = 12
    try:
        only_deg = only_deg
    except Exception:
        only_deg = False

    if not only_deg:
        steps = selfun_multiwhomo(F,dmax,WW)
    else:
        steps = selfun_multiwhomo(F,dmax,WW[0:1])
    G = matrixF5_parallel(F,steps)
