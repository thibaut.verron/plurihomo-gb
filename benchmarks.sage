ncpus = 40

load("helper.sage")
load_notest("examples.sage")
load_notest("matrixF5-mwhomo.sage")

set_random_seed(0)

WW = ((1,2,3),(2,1,1)); dd = [100,50]; npol=2; dmax=190;
# WW = ((1,1,2,3),(2,2,1,1)); dd = [30,30]; npol=2; dmax=71;
# WW = ((1,1,2,3),(2,2,1,1)); dd = [30,30]; npol=3; dmax=85;


K = GF(127)
n = len(WW[0])
nw = len(WW)
R = PolynomialRing(K,n,"x")

F0 = [random_mwhomo(R,WW,dd) for _ in range(npol)]

t0 = time.perf_counter()
steps = algosteps_mwhomo(F0,dmax,WW)
t_prep = time.perf_counter()-t0
t1 = time.perf_counter()
G1 = matrixF5_parallel(F0,steps, algo='F5')
t_gb = time.perf_counter()-t1
t2 = time.perf_counter()
# G0 = matrixF5_parallel(F0,steps, filter_gcd=False,algo='F5')
t_gb_nofilter = time.perf_counter()-t2
dmax_w1=45
steps_w1 = algosteps_mwhomo(F0,dmax_w1,WW[0:1])
t2 = time.perf_counter()
# G2 = matrixF5_parallel(F0,steps_w1, filter_gcd=False,algo='F5')
t_gb_w1 = time.perf_counter()-t2

print("Total time:            {:.2f}".format(t_prep+t_gb))
print("Steps prep:            {:.2f}".format(t_prep))
print("GB comput (filter):    {:.2f}".format(t_gb))
print("GB comput (no filter): {:.2f}".format(t_gb_nofilter))
print("GB comput (default):   {:.2f}".format(t_gb_w1))

