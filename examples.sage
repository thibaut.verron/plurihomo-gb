load("helper.sage")

# Listing monomials
def to_tuple(l):
    # l is an iterable possibly containing iterables, convert recursively to
    # tuples
    try:
        return tuple(to_tuple(x) for x in l)
    except TypeError:
        return l

@cached_function(key=lambda W,d,dmax: (to_tuple(W),d,dmax))
def tuples_of_weighted_degree(W,d, dmax=Infinity):
    """
    return the list of tuples of exponents of W-degree d
    
    INPUT:
    - W in N^n
    - d in N
    - dmax (default: Infinity) - bound on the partial/total (TOFIX) degree of the tuples. Necessary
      if some of the weights are non-positive.

    OUTPUT:
    - list of n-tuples E such that sum of E[i]*W[i] = d

    """
    if len(W) == 0:
        if d == 0:
            return [tuple()]
        else:
            return []
    else:
        res = []
        if W[-1] < 1:
            if dmax is Infinity:
                raise ValueError("With non-positive weights, a degree bound is necessary")
            else:
                dimax = dmax
        else:
            dimax = min(d//W[-1], dmax)
            
        for i in range(dimax+1):
            prev = tuples_of_weighted_degree(W[:-1], d-W[-1]*i, dmax=dmax-i)
            res += [exp + (i,) for exp in prev]
        return res

if not notest:
    tuples_of_weighted_degree([],1)
    tuples_of_weighted_degree([],0)
    tuples_of_weighted_degree([1,1],1)
    tuples_of_weighted_degree([1,1],3)
    tuples_of_weighted_degree([1,2,3],6)

def tuples_of_matrix_weighted_degree(Wlist, dlist, dmax=Infinity):
    n = len(Wlist[0])
    assert all(len(W) == n for W in Wlist)
    assert len(Wlist) == len(dlist)
    assert len(Wlist) > 0
    Mlist = [set(tuples_of_weighted_degree(Wlist[i],dlist[i], dmax=dmax))
             for i in range(len(Wlist))]
    mons = Mlist[0]
    for mset in Mlist[1:]:
        mons.intersection_update(mset)
    res = list(mons)
    res.sort()
    return res


# Generic examples

def random_mwhomo(ring,Wlist,dlist,dmax=Infinity, **rand_kwargs):
    assert all(len(W) == ring.ngens() for W in Wlist), "The length of systems of weights doesn't match the number of variables"
    assert len(Wlist) == len(dlist), "The number of systems of weights doesn't match the number of degrees"
    assert len(Wlist) > 0, "There needs to be at least one system of weights"
    
    Mlist = [set(tuples_of_weighted_degree(Wlist[i],dlist[i], dmax=dmax))
             for i in range(len(Wlist))]
    mons = Mlist[0]
    for mset in Mlist[1:]:
        mons.intersection_update(mset)
    
    coef = ring.base_ring()
    res = ring(0)
    for m in mons:
        res += coef.random_element(**rand_kwargs) * ring.monomial(*m)
    return res

if not notest:
    R.<x,y,z> = QQ[]
    random_mwhomo(R, [[1,1,1], [1,2,3]], [6,6])
    random_mwhomo(R, [[1,1,1], [1,2,3]], [5,6])
    random_mwhomo(R, [[1,1,1], [1,2,3]], [4,6])

    R.<x1,x2,y1,y2> = QQ[]
    random_mwhomo(R,[[1,1,1,1]], [3])
    random_mwhomo(R,[[1,2,1,2]], [4])
    random_mwhomo(R,[[1,1,1,1], [1,2,1,2]], [3,4])

# Test for weighted homogeneity

def is_weighted_homogeneous(f,W):
    if f==0:
        return True
    else:
        mm = f.monomials()
        d = mm[0].weighted_degree(W)
        return all([m.weighted_degree(W) == d for m in mm[1:]])

def _matrix_W_degree_term(t,W):
    return [add(t[i]*w[i] for i in range(len(t))) for w in W]

def matrix_W_degree(f,W):
    return _matrix_W_degree_term(f.lt().exponents()[0], W)

# Cyclic (homogenized)

def ex_cyclic(K,n):
    R = PolynomialRing(K,n+1,"x")
    F = []
    for i in range(1,n-1):
        f = 0
        for j in range(n):
            f += mul(R.gen((j+k) % n) for k in range(i))
        F.append(f)
    F.append(mul(R.gens()[:-1])-R.gen(n)^n)
    return F

if not notest:
    F = ex_cyclic(GF(127),4)
    R = F[0].parent()

# Find good systems of degrees given the weights

def list_degrees(WW,dmax):
    n = len(WW[0])
    all_tuples = add((tuples_of_weighted_degree((1,)*n, d) for d in range(dmax+1)),
                     start=[])
    buckets = {}
    for t in all_tuples:
        dd = tuple(_matrix_W_degree_term(t,WW))
        if dd in buckets:
            buckets[dd] += 1
        else:
            buckets[dd] = 1
    vals = list(buckets.items())
    vals.sort(key=lambda x:x[1])
    vals.reverse()
    return vals

# Homogenization

def add_variables(R,n,names):
    Rh = PolynomialRing(R,n,names)
    new = Rh.gens()
    phi = Rh.flattening_morphism()
    return phi.codomain(), [phi(v) for v in new]

def weighted_homogenize(f,W,h):
    # Aux function, does not change the weights
    d = f.weighted_degree(W)
    R = f.parent()
    return ((h^d)*f([R.gen(i)/h^W[i] for i in range(R.ngens())])).numerator()

if not notest:
    R.<x,y,z> = QQ[]
    f = x^2 + y^2 + x
    Rh,hom = add_variables(R,1,"h")
    h = hom[0]
    weighted_homogenize(f,[1,1,1,1],h)
            
def matrix_weighted_homogenize(f,WW,hh):
    f = hh[0].parent()(f)
    # Deep copy first
    WW = [list(W) for W in WW]
    for i in range(len(WW)):
        for j in range(len(WW)):
            WW[j].append(1 if i==j else 0)
    for i in range(len(WW)):
        f = weighted_homogenize(f,WW[i],hh[i])
    return f,WW

